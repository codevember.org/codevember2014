var MenuLayer = cc.Layer.extend({
	sprite:null,
	ctor:function () {
		//////////////////////////////
		// 1. super init first
		this._super();

		// ask the window size
		var size = cc.winSize;

		// calculate the center point
		var centerpos = cc.p(size.width / 2, size.height / 2);
		
		// font size
		cc.MenuItemFont.setFontSize(40);

		// upper button
		var startItem = new cc.MenuItemSprite(
				new cc.Sprite(res.start_button),
				new cc.Sprite(res.start_button),
				function () {
					cc.log("Master is clicked!");
					cc.director.runScene(new PlayScene());
				}, this);
		startItem.attr({
			x: size.width/2,
			y: size.height/2,
			scale: 0.6,
			anchorX: 0.5,
			anchorY: 0.5,
			color: cc.color(255, 50, 255)
		});

		// add font to menu item
		var menu = new cc.Menu(startItem);
		menu.x = 0;
		menu.y = 0;
		
		// add menu to this
		this.addChild(menu, 1);

		// create a background image and set it's position at the center of the screen
		var spritebg = new cc.Sprite(res.playBG_jpg);
		spritebg.setPosition(centerpos);
		this.addChild(spritebg);	
	}
});

var MenuScene = cc.Scene.extend({
	onEnter:function(){
		this._super();
		var layer = new MenuLayer();
		this.addChild(layer);
		
		//bg music
		cc.audioEngine.playMusic(res.background_menu, true);
		this.scheduleUpdate();
		cc.audioEngine.playEffect(res.background_menu);
	}
});