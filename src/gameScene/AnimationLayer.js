var AnimationLayer = cc.Layer.extend({
	ctor:function (space) {
		this._super();
		this.space = space;
		var player = this.init();
		
		// call update on every frame
		this.scheduleUpdate();
		
		// todo: structure code
		var list = setupListener(player, this);
		
		cc.eventManager.addListener(list.mouse, this);
		cc.eventManager.addListener(list.touch, this);
	},
	init:function () {
		this._super();

		//1. create PhysicsSprite with a sprite frame name
		this.sprite = new cc.PhysicsSprite(res.runner);
		var contentSize = this.sprite.getContentSize();
		// 2. init the runner physic body
		this.body = new cp.Body(1, cp.momentForBox(1, contentSize.width, contentSize.height));
		//3. set the position of the runner
		this.body.p = cc.p(global.g_runnerStartX, global.g_groundHeight + contentSize.height / 2);
		//4. apply impulse to the body
		this.body.applyImpulse(cp.v(0, 0), cp.v(0, 0));//run speed
		//5. add the created body to space
		this.space.addBody(this.body);
		//6. create the shape for the body
		this.shape = new cp.BoxShape(this.body, contentSize.width - 14, contentSize.height);
		//7. add shape to space
		this.space.addShape(this.shape);
		//8. set body to the physic sprite
		this.sprite.setBody(this.body,1,123);
		
		this.addChild(this.sprite, 1, 123);

		var draw = new cc.DrawNode();
		this.addChild(draw, 10);


		var map = new Map("1337seed");
		cc.log(map.path);
		draw.drawPoly(map.path, null, 2, cc.color(255, 0, 255, 255));
		
		var fire = cc.ParticleFire.createWithTotalParticles(199);
		fire.setScaleX(4.2);
		fire.setScaleY(0.3);
		fire.setPosition(cc.p(-150,40));
		
		this.addChild(fire,-1,12);
		
		return this.body;
	},
	jump : 0,
	update : function(dt) {
		// update meter
		var statusLayer = this.getParent().getChildByTag(1337);
		statusLayer.updateMeter();
		
		if(this.jump > 0)
			this.jump--;
	}
});