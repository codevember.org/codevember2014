var GameMenuLayer = cc.LayerColor.extend({
	// constructor
	ctor:function () {
		this._super();
		this.init();
	},
	init:function () {
		// darken the background
		this._super(cc.color(0, 0, 0, 180));
		
		// winsize, center and fontsize
		var winSize = cc.director.getWinSize();
		var centerPos = cc.p(winSize.width / 2, winSize.height / 2);
		cc.MenuItemFont.setFontSize(30);
		
		// the button to continue
		var menuItemContinue = new cc.MenuItemSprite(
				new cc.Sprite(res.resume_button),
				new cc.Sprite(res.resume_button),
				this.onContinue, this);
		menuItemContinue.attr({
			x: 250,
			y: 50,
			scale: 0.4
		});
		
		// the button to restart
		var menuItemRestart = new cc.MenuItemSprite(
				new cc.Sprite(res.restart_button),
				new cc.Sprite(res.restart_button),
				this.onRestart, this);
		menuItemRestart.attr({
			x: 0,
			y: 50,
			scale: 0.4
		});
		
		// the button to go back to menu
		var menuItemMenu = new cc.MenuItemSprite(
			new cc.Sprite(res.menu_button),
			new cc.Sprite(res.menu_button),
			this.onMenu, this);
		menuItemMenu.attr({
			x: -250,
			y: 50,
			scale: 0.4
		});
		
		// the menu, add restart and menu
		var menu = new cc.Menu(menuItemRestart);
		menu.addChild(menuItemContinue, 5, 1338)
		menu.addChild(menuItemMenu, 4, 1337);
		menu.setPosition(centerPos);
		
		// add menu to scene
		this.addChild(menu, 1, 200);
	},
	onRestart:function (sender) {
		cc.director.resume();
		this.removeFromParent(true);
		cc.director.runScene(new PlayScene);
	},
	onContinue:function (sender){
		cc.director.resume();
		this.removeFromParent(true);
	},
	onMenu:function (sender){
		cc.director.resume();
		cc.director.runScene(new MenuScene);
	}
});