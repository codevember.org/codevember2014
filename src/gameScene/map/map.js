Map = cc.Node.extend({
	path:null,
	ctor:function(seed){
		var rng = new Math.seedrandom(seed);

		this.modules = [ 
			new Gap(rng),
			new Cube(rng)
		];
		var vertices = [cc.p(0, global.g_groundHeight)];
		
		for (var i = 0; i < 1200;i = vertices[vertices.length - 1].x) {
			var index = Math.round(rng() * (this.modules.length - 1));
			var module = this.modules[index];
			vertices = vertices.concat(module.generatePath(i));
		};
		this.path = vertices;
	},
	modules : null
});