var StartUpLayer = cc.Layer.extend({
	sprite:null,
	ctor:function () {
		//////////////////////////////
		// super init first
		this._super();
		
		// ask the window size
		var size = cc.winSize;
		
		// font size
		cc.MenuItemFont.setFontSize(70);
		
		// Start Item 
		var startItem = new cc.MenuItemFont(
				"Start the Game",
				function () {
					cc.log("Master is clicked!");
					cc.director.runScene(new MenuScene());
				}, this);
		startItem.attr({
			anchorX: 0.5,
			anchorY: 0.5
		});
		// center the font
		startItem.setPosition(size.width/2, size.height/2);
		
		// add font to menu item
		var menu = new cc.Menu(startItem);
		menu.x = 0;
		menu.y = 0;
		
		// add menu to this
		this.addChild(menu, 1);
		
		// add background image
		this.sprite = new cc.Sprite(res.helloBG_jpg);
		this.sprite.attr({
			x: size.width/2,
			y: size.height/2
		});
		this.addChild(this.sprite, 0);
	}
});

var StartUpScene = cc.Scene.extend({
	onEnter:function(){
		this._super();
		var layer = new StartUpLayer();
		this.addChild(layer);
	}
});

