var res = {
	// backgrounds
	bg_sky : "res/background/sky.jpg",
	bg_clouds : "res/background/clouds.png",
	bg_clouds2 : "res/background/clouds2.png",
    helloBG_jpg : "res/helloBG.jpg",
    menu_simple_jpg : "res/menu_simple.jpg",
    playBG_jpg : "res/playBG.jpg",
    
    // charakter
    runner : "res/runner.png",
    
    // buttons
    start_button : "res/button1-start.png",
    highscore_button : "res/button1-highscore.png",
   	restart_button : "res/button1-restart.png",
   	resume_button : "res/button1-resume.png",
   	menu_button : "res/button1-menu.png",
   	pause_button : "res/button1-pause.png",
   	
   	// wav
   	background_menu : "res/sounds/bg_music_loop.wav"
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}