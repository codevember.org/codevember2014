# Codevember

Just a few words on how to set up the project


## Getting started

You need the Cocos2d-JS Framework v3.1 and the Code IDE (both available under http://www.cocos2d-x.org/download).

After installing the IDE, choose your JS Framework path. Clone this repository
into your workspace and open your IDE. Right click into the project explorer (in the IDE), then choose "import cocos project" and click okay.

Build runtime first and generate the missing files, after that you are ready to compile.

(todo: how to start project on linux (tobi, jens?))

## Docs

API Reference: http://www.cocos2d-x.org/reference/html5-js/V3.0/index.html
Gettings started: http://www.cocos2d-x.org/wiki/Getting_Started_Cocos2d-js


## Basic workflow
Your workflow should roughly follow this example, performing the following steps:

    # Fetch changes and attempt to automatically merge into local copy
    git pull
    # If for some reason this does not work (which it will if were always committing cleanly) use
    git fetch <filenames>
    # which will download the specified files. You will have to merge by hand
    
    # For **each** file you are changig, do the following
    git add <filename>
    git commit -m"describe the changes/fixes/additions briefly"
    
    # When you're done, simply
    git push

This should **always** be the same workflow, which ensures we won't run into any problems

## Advanced workflow
If you want to try something and don't want to mess with the current branch (by default, you're working on `master`), you should create a new branch.
To do so, use

    git branch <new branch name>
    git checkout <branchname>

As long as you are working on this new branch, all changes are commited to the `HEAD` of this branch. By the time you decide to merge it into `master` do

    git checkout master
    git merge <otherbranch>
    git push

This should work (hopefully, I didn't look it up, so maybe there are some mistakes in there :P)

## Misc

Folder structure:
/raw/ - for raw files like sounds and images
/src/ - for project source files
/res/ - used (image) files